import { Worker } from "./modules/worker.js";
import { Bank } from "./modules/bank.js";
import { loadComputers, getComputers, getComputer, updateComputers, getSelectedComputer, selectComputer } from "./modules/computers.js";
import "./modules/html-utils.js";
import { buyButtonClick, getComputerSelectionElement, loanButtonClick, onComputerChanged, payDebtClick, populateDropdownList, storeButtonClick, updateComputerElement, workButtonClick } from "./modules/html-utils.js";

//urls :
const storeUrl = "https://hickory-quilled-actress.glitch.me/";
const computersFolder = "computers";
const computerImagesFolder = "assets/images/";

//objects :
const worker = new Worker("Santa", 100);
const bank = new Bank("Bank", 0.1);
loadComputersAndUpdateHtml();

//subscribe functionality to events:
workButtonClick(() => worker.work());
storeButtonClick(() => bank.store(worker.withdraw()));
loanButtonClick(() => bank.promptLoan("Loan amount"));
buyButtonClick(() => {
    if(bank.withdraw(getSelectedComputer().price)){
        //enough balance to buy
        window.alert("Congratulations! You are now the owner of " + getSelectedComputer().title);

        //stock - 1 and remove form list if 0
        getSelectedComputer().stock -= 1;

        if(getSelectedComputer().stock <= 0)
        {
            getSelectedComputer().active = false;
            updateComputers();
            updateComputersHtml();
        }
        
        updateComputerElement(getSelectedComputer(), storeUrl);
    }
    else
    {
        window.alert("You cannot afford this computer!");
    }
});

//Withdraw from salary to pay debt -> if we exceed the debt -> store the rest in bank
payDebtClick(() => bank.store(bank.payDebt(worker.withdraw()).excess));

onComputerChanged(() => updateSelectedComputer(getComputerSelectionElement().selectedIndex));

//functions:

async function loadComputersAndUpdateHtml(){ 
    await loadComputers(getComputersUrl());    
    updateComputersHtml();
}

function updateComputersHtml(){
    populateDropdownList(getComputers());
    updateComputerElement(getSelectedComputer(), storeUrl);
}
function getComputersUrl(){ return storeUrl.concat(computersFolder); }
function getStoreAssetsUrl(){ return storeUrl.concat(computerImagesFolder);}

function updateSelectedComputer(index){
    selectComputer(index);
    updateComputerElement(getSelectedComputer(), storeUrl);
}