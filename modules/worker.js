
import { updateWorkerName, updateSalaryElement } from "./html-utils.js";

export class Worker {
    #name
    #salary
    #income

    constructor(name, income){
        this.#name = name;
        this.#salary = 0;
        this.#income = income;

        this.updateHtml();
        updateWorkerName(this.#name);
    }
    
    //adds income to salary
    work = function(){
        this.#salary += this.#income;
        this.updateHtml();
    }

    //returns the amount banked
    withdraw = function(){
        let banking = this.#salary;
        this.#salary = 0;
        this.updateHtml();
        return banking;
    }

    updateHtml = function(){
        updateSalaryElement(this.#salary);
    }
}