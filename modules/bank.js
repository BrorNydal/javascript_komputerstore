
import { updateBankName, updateBalanceElement, updateDebtElement } from "./html-utils.js"

export class Bank 
{
    #name
    #balance
    #debt
    #loanDeduction

    constructor(name, deduction)
    {
        this.#name = name;
        this.#balance = 0;
        this.#debt = 0;
        this.#loanDeduction = deduction;

        this.updateHtml();
        updateBankName(this.#name);
    }

    store = function(amount){
        if(amount > 0)
        {
            if(this.hasDebt())
            {
                amount -= this.payDebt(amount * this.#loanDeduction).paid;        
            }    
        
            this.#balance += amount;  
            this.updateHtml(); 
        }
    }

    loan = function(amount){
        if(amount <= this.#balance * 2)
        {
            this.#balance += amount;
            this.#debt += amount;
            this.updateHtml();
        }
        else
        {
            this.promptLoan("Please enter an amount below your balance multipled by 2");        
        }
    }

    //Withdraw, returns true if able
    withdraw = function(amount){

        let withdrawSuccess = false;

        if(amount <= this.#balance)
        {
            withdrawSuccess = true;
            this.#balance -= amount;
            this.updateHtml();
        }

        return withdrawSuccess;
    }

    payDebt = function(amount){
        let paid = 0;
        let excess = 0;

        if(amount > this.#debt){
            paid = this.#debt;
            excess = amount - this.#debt;
            this.#debt = 0;
        }
        else{
            this.#debt -= amount;
            paid = amount;
        }

        //TODO: Update display based on if we have debt left or not
        this.updateHtml();
        return {'paid': paid, 'excess': excess};
    }

    hasDebt = function() { return this.#debt > 0; }

    // withdraw = function(amount) { 
    //     if(amount <= this.balance)
    //     {
    //         this.balance -= amount;
    //         updateHtml();
    //         return amount;
    //     }

    //     return 0;
    // }

    promptLoan = function(message){ 
        if(!this.hasDebt())
            this.loan(Number(window.prompt(`${message} :`, 0)));
    }
    updateHtml = function(){
        updateBalanceElement(this.#balance);
        updateDebtElement(this.#debt, this.hasDebt());
    }
}