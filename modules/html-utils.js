
//This script contains all functionality for affecting the html values

//HTML read elements
const salaryElement = document.getElementById("worker-salary");
const debtElement = document.getElementById("bank-debt");
const balanceElement = document.getElementById("bank-balance");
const computerElement = document.getElementById("computer");
const storeSelectionElement = document.getElementById("store-selection");
const storeSpecsElement = document.getElementById("store-specs");
const computerPriceElement = document.getElementById("computer-price");

//HTML input elements
const btnWorkElement = document.getElementById("worker-btn-work");
const btnStoreElement = document.getElementById("worker-btn-store");
const btnPayLoanElement = document.getElementById("worker-btn-pay-debt");
const btnLoanElement = document.getElementById("bank-btn-loan");
const btnBuyElement = document.getElementById("computer-btn-buy");

export function getComputerSelectionElement() { return storeSelectionElement; }

export function updateBankName(name) {document.getElementById("bank-name").innerHTML = name;}
export function updateWorkerName(name) {document.getElementById("worker-name").innerHTML = name;}
export function updateSalaryElement(newValue)  { salaryElement.innerHTML = `Salary: ${Number(newValue)}`; }
export function updateDebtElement(newValue, hasDebt)    { 
    if(hasDebt)
    {
        debtElement.innerHTML = `Debt: ${Number(newValue)}`; 
        btnLoanElement.style = "display: none;";
        btnPayLoanElement.style = "display: block;";
    }
    else{
        debtElement.innerHTML = "";
        btnLoanElement.style = "display: block;";
        btnPayLoanElement.style = "display: none;";
    }
}
export function updateBalanceElement(newValue) { balanceElement.innerHTML = `Balance: ${Number(newValue)}`; }

//updates everything related to a new computer being selected
export async function updateComputerElement(computer, url) {
    const specs = computer.specs;
    let htmlSpecString = "";
    for(let spec of specs) htmlSpecString += `<p style="font-size:smaller; margin-top: 1px; margin-bottom: 1px;">${spec}</p> \n`;

    let imageUrl = url + computer.image;

    let imageUrlCheck = await fetch(imageUrl);
    
    if(imageUrlCheck.status != 200)
        imageUrl = "laptop_placeholder.png"    

    let specHeight = 180;

    computerElement.innerHTML = 
    `
    <div style="margin: 40px;">
        <img src="${imageUrl}" alt="laptop" class="laptop-img" style="float: left; width:${specHeight}px; height: ${specHeight}px;">
    </div>    
    <div style="justify-content: center; margin-left: 60px; float: left;">
        <h3> ${computer.title} </h3>
        <p id="store-laptop-description"> ${computer.description}</p>        
        <p id="store-laptop-stock"> In stock : ${computer.stock}</p>
    </div>    
    `;

    computerPriceElement.innerHTML = 
    `
        <h3 id="store-laptop-price"> ${computer.price} NOK </h3>
    `;

    storeSpecsElement.innerHTML = htmlSpecString;
}

//populates that dropdown list that lets the user select a computer
export function populateDropdownList(computers)
{
    storeSelectionElement.innerHTML = "";

    for(let computer of computers)
    {
        let element = document.createElement("option");
        element.textContent = computer.title;
        element.value = computer.title;

        storeSelectionElement.append(element);
    }
}



//event listeners:

export function workButtonClick(listener){
    btnWorkElement.addEventListener("click", listener);
}
export function storeButtonClick(listener){
    btnStoreElement.addEventListener("click", listener);
}
export function loanButtonClick(listener){
    btnLoanElement.addEventListener("click", listener);
}
export function payDebtClick(listener){
    btnPayLoanElement.addEventListener("click", listener);
}
export function buyButtonClick(listener){
    btnBuyElement.addEventListener("click", listener);
}
export function onComputerChanged(listener){
    storeSelectionElement.onchange = listener;
}
