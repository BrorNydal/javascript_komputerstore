
//everything for handling the computers

let computers = [];
let selectedComputer = 0;

export async function loadComputers(url){
    try{
        let result = await fetch(url);
        computers = await result.json(); //await?        
    }
    catch(error)
    {
        console.log(`loadStore() error! ${error}`);
    }
}

//Removes any computer out of stock, updates the list of computers
export function updateComputers(){
    
    let temp = [];

    for(let comp of computers)
    {
        if(comp.active === true)
            temp.push(comp);
    }

    computers = temp;
}

export function selectComputer(at) { selectedComputer = at; }
export function getSelectedComputer(){ return computers[selectedComputer];}
export function getComputers(){ return computers; }
export function getComputer(index){return computers[index];}